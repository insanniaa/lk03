/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas pertambahan dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa interface akan lebih cocok digunakan daripada abstract class?
 * "Jawab disini..." 
 * yaitu pada kondisi jika salah satu class atau beberapa classnya dalam satu package yang sama memiliki karakteristik yang khusus, maka karakteristik tersebut dapat disembunyikan di enkapsulasi jadi sebuah interface. Misalnya, ketika ingin membuat kalkulator dengan operasi matematika yang berbeda beda seperti pertambahan, pengurangan, perkalian, dan pembagian yang menggunakan operator matematika yang berbeda-beda serta pada angka yang berbeda-beda. Maka dalam hal ini, interface membantu untuk membuat berbagai class yang mengimplemen interface tersebut dengan operasi matematika yang berbeda beda, cara yang berbeda-beda, dan jenis data yang berbeda-beda.
 * 
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operan1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operan2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        double result = kalkulator.hitung(operan1, operan2);
        System.out.println("Result: " + result);
    }
}
abstract class Kalkulator {
    double operan1, operan2;

    // Do your magic here...
    public void setOperan1(double operan1) {
        this.operan1 = operan1;
    }

    public double getOperan1() {
        return operan1;
    }

    public void setOperan2(double operan2) {
        this.operan2 = operan1;
    }

    public double getOperan2() {
        return operan2;
    }

    public abstract double hitung();

    public abstract void setOperan(double operan1, double operan2);

}
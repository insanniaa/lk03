class Pengurangan extends Kalkulator {
    // Do your magic here...

    public Pengurangan() {
        setOperan(operan1, operan2);
    }

    public void setOperan(double operan1, double operan2) {
        this.operan1 = operan1;
        this.operan2 = operan2;
    }

    @Override
    public double hitung() {
        return operan1 - operan2;
    }

}